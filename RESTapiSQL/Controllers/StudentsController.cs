﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using RESTapiSQL.Model;
using System.ComponentModel.DataAnnotations;
using RESTapiSQL.Helper;
using Newtonsoft.Json;
using RESTapiSQL.Interface;

namespace RESTapiSQL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        IConfiguration configure;
        IStudentService _IStudentService;

       
        public StudentsController(IConfiguration configure, IStudentService iStudentService)
        {
            this.configure = configure;
            _IStudentService = iStudentService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_IStudentService.GetAllStudentList());
        }

        [HttpGet("{id}")]
        public IActionResult GetStudentDetail(int id)
        {

            return Ok(_IStudentService.GetStudentById(id));          
        }
    

        [HttpGet("joins/list")]
        public IActionResult GetJoins()
        {
            
            string query = @"SELECT StudentId, StudentName, DateOfBirth as DOB, GenderName, DepartmentName, MarksPercentage FROM [dbo].[Student] inner join [dbo].[Gender] on Student.GendId = Gender.GenderId 
              inner join [dbo].[Department] on Student.DepId = Department.DepartmentId";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        
        [HttpGet("joins/Average/list")]
        public IActionResult GetAverageJoins(int id)
        {
            
                string query = @"SELECT StudentId, StudentName, DateOfBirth as DOB, GenderName, DepartmentName, MarksPercentage FROM [dbo].[Student] inner join [dbo].[Gender] on Student.GendId = Gender.GenderId 
              inner join [dbo].[Department] on Student.DepId = Department.DepartmentId
              where MarksPercentage > '" + id + "'";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }

        }

        [HttpGet("joins/Gender/list")]
        public IActionResult GetGenderJoins(int id)
        {
            
            string query = @"SELECT StudentId, StudentName, DateOfBirth as DOB, GenderName, DepartmentName, MarksPercentage FROM [dbo].[Student] inner join [dbo].[Gender] on Student.GendId = Gender.GenderId 
              inner join [dbo].[Department] on Student.DepId = Department.DepartmentId
              where GenderId = '" + id + "'";
            
            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        [HttpGet("joins/Department/list")]
        public IActionResult GetDepartmentJoins(int id)
        {
            
            string query = @"SELECT StudentId, StudentName, DateOfBirth as DOB, GenderName, DepartmentName, MarksPercentage FROM [dbo].[Student] inner join [dbo].[Gender] on Student.GendId = Gender.GenderId 
              inner join [dbo].[Department] on Student.DepId = Department.DepartmentId
              where DepartmentId = '" + id + "'";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }

        }

        [HttpGet("joins/student/count")]
        public IActionResult GetStudentCount()
        {
            
            string query = "SELECT COUNT(*) AS NoOfStudent FROM STUDENT";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        [HttpGet("list/sorting")]
        public IActionResult GetSortStudents(string orderByName, string orderByType)
        {
            
            string query = @"select * from student order by "+orderByName+" "+orderByType+"";
            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        [HttpGet("list/filtering")]
        public IActionResult GetFilterStudents(string filterbyName, string searchValue)
        {
            
            string query = @"select * from student where "+filterbyName+" like '" + searchValue + "'";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        [HttpGet("list/pagination")]
        public IActionResult GetPaginationStudents(int pageNumber, int rowsOfPage)
        {
            
            string query = @"DECLARE @PageNumber AS INT
                             DECLARE @RowsOfPage AS INT
                             SET @PageNumber = '" + pageNumber + @"'
                             SET @RowsOfPage = '" + rowsOfPage + @"'
                             SELECT StudentName, MarksPercentage FROM Student
                             ORDER BY MarksPercentage
                             OFFSET(@PageNumber - 1) * @RowsOfPage ROWS
                             FETCH NEXT @RowsOfPage ROWS ONLY";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }


        [HttpPost]
        public IActionResult Post([FromBody] Student studentNew)
        {
            return Ok(_IStudentService.AddStudent(studentNew));
           //DateTime date = Convert.ToDateTime(studentNew.DateOfBirth);    
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Student studentModified)
        {
            return Ok(_IStudentService.UpdateModel(id, studentModified));           
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return Ok(_IStudentService.DeleteStudent(id));
        }


    }
}
