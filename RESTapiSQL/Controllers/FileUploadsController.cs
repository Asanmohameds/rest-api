﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RESTapiSQL.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RESTapiSQL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileUploadsController : ControllerBase
    {
        private static IWebHostEnvironment _webHostEnvironment;

        public FileUploadsController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost("[action]")]
        public async Task<string> Upload([FromForm] UploadImage imgObj)
        {
            if (imgObj.Files.Length>0)
            {
                try
                {
                    if (!Directory.Exists(_webHostEnvironment.WebRootPath + "\\Images\\"))
                    {
                        Directory.CreateDirectory(_webHostEnvironment.WebRootPath + "\\Images\\");
                    }

                    using(FileStream fileStream = System.IO.File.Create(_webHostEnvironment.WebRootPath + "\\Images\\" + imgObj.Files.FileName))
                    {
                        imgObj.Files.CopyTo(fileStream);
                        fileStream.Flush();
                        return "\\Images\\" + imgObj.Files.FileName;
                    }

                }
                catch(Exception ex)
                {
                    return ex.Message.ToString();
                }
            }
            else
            {
                return "Please provide the file to upload";
            }
        }

    }
}
