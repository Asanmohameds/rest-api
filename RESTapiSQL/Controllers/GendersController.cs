﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RESTapiSQL.Helper;
using RESTapiSQL.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace RESTapiSQL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GendersController : ControllerBase
    {
        IConfiguration configure;
        public GendersController(IConfiguration configuration)
        {
            configure = configuration;
        }

        [HttpGet]
        public IActionResult Get()
        {
            string query = @"select GenderId, GenderName from dbo.Gender";

            //** connect to the sql server database.
            string serverPath = configure.GetConnectionString("Database");

            //** need to install system.data.sqlclient
            SqlDataReader reader;

            DataTable table = new DataTable();

            using (SqlConnection con = new SqlConnection(serverPath))
            {
                con.Open();

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    reader = cmd.ExecuteReader();
                    table.Load(reader);
                    con.Close();
                    reader.Close();
                }
                return new JsonResult(table);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetGenderDetail(int id)
        {
            string query = "select * from dbo.Gender where GenderId = '" + id + "'";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

            [HttpPost]
        public IActionResult Post([FromBody] Gender genderNew)
        {
            string query = @"INSERT INTO Gender 
                            (GenderName) 
                                values
                                 (
                                    '" + genderNew.GenderName + @"'  
                                  )";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Gender genderModified)
        {

            string query = @"UPDATE Gender SET 
                                GenderName='" + genderModified.GenderName + @"',
                                  where GenderId=" + id + @"
                                  ";
            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            string query = @"
                                delete from dbo.Gender
                                  where GenderId=" + id + @"
                                  ";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

    }
}
