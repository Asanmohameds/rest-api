﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RESTapiSQL.Helper;
using RESTapiSQL.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace RESTapiSQL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        IConfiguration configure;
        public DepartmentsController(IConfiguration configuration)
        {
            configure = configuration;
        }

        [HttpGet]
        public IActionResult Get()
        {
            string query = @"select DepartmentId, DepartmentName from dbo.Department";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetDepartmentDetail(int id)
        {
            string query = "select * from dbo.Department where DepartmentId = '" + id + "'";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] Department departmentNew)
        {

            string query = @"INSERT INTO Department 
                            (DepartmentName) 
                                values
                                 (
                                    '" + departmentNew.DepartmentName + @"'  
                                  )";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
        }

        [HttpPut]
        public IActionResult Put([FromBody] Department departmenModified)
        {           
            string query = @"UPDATE Department SET 
                                DepartmentName='" + departmenModified.DepartmentName + @"'
                                  where DepartmentId=" + departmenModified.DepartmentId + @"
                                  ";
            //** need to install system.data.sqlclient
            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);
                return data;
            }
            catch (Exception e)
            {
                return new JsonResult(e);
            }
 
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            string query = @"
                                delete from dbo.Department
                                  where DepartmentId=" + id + @"
                                  ";
            try
            {
                var data = new Helping(configure).ExecuteGetQuery(query);
                return new JsonResult("Deleted Successfully");
            }
            catch(Exception e)
            {
                return new JsonResult(e);
            }
        }


       


    }
}
