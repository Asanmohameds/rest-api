﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RESTapiSQL.Helper;
using RESTapiSQL.Interface;
using RESTapiSQL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RESTapiSQL.ServiceClasses
{
    public class StudentServices : IStudentService
    {
        private IConfiguration configure;
        public StudentServices(IConfiguration configuration)
        {
            configure = configuration;
        }

        public List<Student> GetAllStudentList()
        {
            string query = @"select StudentID, StudentName, GendId, DepId, format(DateOfBirth, 'dd/MM/yyyy') as DOB, MarksPercentage from dbo.Student";

            //JsonResult data = null;
            try
            {
                var data = new Helping(configure).ExecuteGetQuery(query);

                var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(data.Value));
                return source;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Student> GetStudentById(int id)
        {
            string query = "select * from dbo.Student where StudentId = '" + id + "'";

            JsonResult data = null;
            try
            {
                data = new Helping(configure).ExecuteGetQuery(query);

                var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(data.Value));
                return source;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Student> AddStudent([FromBody] Student studentNew)
        {
            string query = @"INSERT INTO Student 
                            (StudentName,DateOfBirth, MarksPercentage, GendId, DepId ) 
                                values
                                 (
                                    '" + studentNew.StudentName + @"'
                                    ,'" + studentNew.DateOfBirth + @"'
                                    ,'" + studentNew.MarksPercentage + @"'
                                    ,'" + studentNew.GendId + @"'
                                    ,'" + studentNew.DepId + @"'
                                  )";

            try
            {
                var data = new Helping(configure).ExecutePostQuery(query);
                var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(data.Value));
                return source;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Student> UpdateModel(int id, [FromBody] Student studentModified)
        {
            string query = @"UPDATE STUDENT SET 
                                StudentName='" + studentModified.StudentName + @"',
                                DateOfBirth='" + studentModified.DateOfBirth + @"',
                                MarksPercentage='" + studentModified.MarksPercentage + @"',
                                GendId='" + studentModified.GendId + @"',
                                DepId='" + studentModified.DepId + @"'
                                  where StudentId=" + id + @"
                                  ";
            //JsonResult data = null;
            try
            {
                var data = new Helping(configure).ExecuteGetQuery(query);
                var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(data.Value));
                return source;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Student> DeleteStudent(int id)
        {
            string query = @"
                                delete from dbo.Student
                                  where StudentId=" + id + @"
                                  ";

            //JsonResult data = null;
            try
            {
              var data = new Helping(configure).ExecuteGetQuery(query);
                var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(data.Value));
                return source;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
