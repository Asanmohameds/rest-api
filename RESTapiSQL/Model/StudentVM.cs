﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RESTapiSQL.Model
{
    public class StudentVM : Student 
    {

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }


        public int GenderId { get; set; }
        public string GenderName { get; set; }
    }
}
