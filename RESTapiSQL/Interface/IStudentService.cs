﻿using Microsoft.AspNetCore.Mvc;
using RESTapiSQL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RESTapiSQL.Interface
{
    public interface IStudentService
    {
        //IEnumerable<Student> GetAllStudentList();

        List<Student> GetAllStudentList();
        List<Student> GetStudentById(int id);
        List<Student> AddStudent([FromBody] Student studentNew);
        List<Student> UpdateModel(int id, [FromBody] Student studentModified);
        List<Student> DeleteStudent(int id);

    }
}
