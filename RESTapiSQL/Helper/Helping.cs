﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace RESTapiSQL.Helper
{
    public class Helping
    {
        IConfiguration configure;

        public Helping(IConfiguration configuration)
        {
            configure = configuration;
        }

        public JsonResult ExecuteGetQuery(string query)
        {
            string serverPath = configure.GetConnectionString("Database");
            //** need to install system.data.sqlclient
            SqlDataReader reader;

            DataTable table = new DataTable();

            using (SqlConnection con = new SqlConnection(serverPath))
            {
                con.Open();

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    reader = cmd.ExecuteReader();
                    table.Load(reader);
                    con.Close();
                    reader.Close();
                }
                return new JsonResult(table);

            }
        }

        public JsonResult ExecutePostQuery(string query)
        {
            string serverPath = configure.GetConnectionString("Database");
            //** need to install system.data.sqlclient

            using (SqlConnection con = new SqlConnection(serverPath))
            {
                con.Open();

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.ExecuteScalar();
                    con.Close();
                }

            }

            string lastUpdatedQuery = @"Select IDENT_CURRENT('Student') as lastGenIdColumn";

            string _servPath = configure.GetConnectionString("Database");
            //** need to install system.data.sqlclient
            SqlDataReader reader;

            DataTable table = new DataTable();

            using (SqlConnection con = new SqlConnection(_servPath))
            {
                con.Open();

                using (SqlCommand cmd = new SqlCommand(lastUpdatedQuery, con))
                {
                    reader = cmd.ExecuteReader();
                    table.Load(reader);
                    con.Close();
                    reader.Close();
                }
                return new JsonResult(table);
            }
        }
    }
}
